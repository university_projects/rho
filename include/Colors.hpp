#ifndef RHO_COLORS_HPP
#define RHO_COLORS_HPP

#define RESET   "\x1B[0m"               ///< Resets the output
#define BOLD(x) "\x1B[1m" x RESET       ///< Writes x as bold
#define GREEN   "\x1B[32m"              ///< Paints the output in green

#endif // RHO_COLORS_HPP
