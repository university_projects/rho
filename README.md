# RHO #

Rho is a simple math operations interpreter built for learning purposes which works in the terminal. Written in C++11 without any dependency.

### Feadtures ###
Tokenizer:

* Read operands like [0-9]*.?[0-9]*
* Read operatos +, -, * and /

### Contributors ###
* Alberto Castro Estrada
* Mildred Samantha Hernández Torres