#include <iostream>
#include <string>
#include "Colors.hpp"
#include "Tokenizer.hpp"

int main()
{
    while (1)
    {
        std::string instruction;
        std::cout << BOLD(GREEN"rho > ");

        std::getline(std::cin, instruction);
        std::cin.clear();

        if (instruction == "q" || instruction == "quit" || instruction == "exit")
            return 0;
        else if (! instruction.empty())
        {
            if (Tokenizer::tokenize(instruction))
            {
                std::cout << "Operands: ";
                for (auto &operand : Tokenizer::getOperands())
                    std::cout << operand << " ";

                std::cout << "\nOperators: ";
                for (auto &operat : Tokenizer::getOperators())
                    std::cout << operat << " ";
            }
            else
            {
                std::cout << Tokenizer::getErrorMessage();
            }

            Tokenizer::clear();
        }

        std::cout << std::endl;
    }
}
