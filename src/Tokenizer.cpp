#include "Tokenizer.hpp"
#include <iostream>
#include <cctype>

///////////////////////////////////////
Tokenizer::Tokenizer() : error(false)
{
}

///////////////////////////////////////
Tokenizer::~Tokenizer()
{
}

///////////////////////////////////////
Tokenizer & Tokenizer::getInstance()
{
    static Tokenizer tokenizer;
    return tokenizer;
}

///////////////////////////////////////
const std::vector <float> & Tokenizer::getOperands()
{
    return getInstance().operands;
}

///////////////////////////////////////
const std::vector <char> & Tokenizer::getOperators()
{
    return getInstance().operators;
}

///////////////////////////////////////
const std::string & Tokenizer::getErrorMessage()
{
    return getInstance().errorMessage;
}

///////////////////////////////////////
void Tokenizer::clear()
{
    auto &tokenizer = getInstance();
    tokenizer.operands.clear();
    tokenizer.operators.clear();
    tokenizer.error = false;
    tokenizer.errorMessage.clear();
}

///////////////////////////////////////
bool Tokenizer::tokenize(std::string &instruction)
{
    auto &tokenizer = Tokenizer::getInstance();
    auto it = instruction.begin();
    auto end = instruction.end();

    //////////////////////// Reading the first operand
    it = tokenizer.readOperand(it, end);
    if (tokenizer.error)
        return false;

    while (it != end)
    {
        it = tokenizer.readOperator(it, end);
        if (tokenizer.error)
            return false;

        it = tokenizer.readOperand(it, end);
        if (tokenizer.error)
            return false;

        it = tokenizer.readEmptySpaces(it, end);
    }

    return true;
}

///////////////////////////////////////
std::string::iterator Tokenizer::readEmptySpaces(std::string::iterator &it, const std::string::const_iterator &end) const
{
    while (it != end && *it == ' ')
        ++it;

    return it;
}

///////////////////////////////////////
std::string::iterator Tokenizer::readOperand(std::string::iterator &it, const std::string::const_iterator &end)
{
    it = readEmptySpaces(it, end);

    std::string operand;
    bool foundDot = false;

    while (it != end)
    {
        if (std::isdigit(*it))
        {
            operand.push_back(*it);
            ++it;
        }
        else if (*it == '.')
        {
            if (! foundDot)
            {
                operand.push_back(*it);
                foundDot = true;
                ++it;
            }
            else
            {
                error = true;
                errorMessage = "Error reading operand, found double dot";
                return it;
            }
        }
        else
        {
            if (operand.empty())
            {
                error = true;
                errorMessage = "Error reading operand, operand is empty";
            }
            break;
        }
    }

    if (operand.empty())
    {
        error = true;
        errorMessage = "Error reading operand, operand is empty";
    }
    else if (operand == ".")
    {
        error = true;
        errorMessage = "Error reading operand, '.' is an invalid operand. Use 0. or .0 instead";
    }
    else
        operands.push_back(std::stof(operand));

    return it;
}

///////////////////////////////////////
std::string::iterator Tokenizer::readOperator(std::string::iterator &it, const std::string::const_iterator &end)
{
    it = readEmptySpaces(it, end);
    if (it != end)
    {
        if (*it == '+' || *it == '-' || *it == '*' || *it == '/')
        {
            operators.push_back(*it);
            ++it;
        }
        else
        {
            error = true;
            errorMessage = "Error reading operator, ";
            errorMessage += *it;
            errorMessage += " is an invalid operator";
        }
    }
    else
    {
        error = true;
        errorMessage = "Error reading operator, operator not found";
    }

    return it;
}
